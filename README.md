# Developers Tools

This repo contains a set of tools and configuration files used for development.
Especially for Android Rom development

## Install tool
First get git submodule:
```
git submodule update --init
```

then you can use the vim configuration file
```
 ln -s PATH_TO_THIS_REPO/.vim ~/.vim
 ln -s PATH_TO_THIS_REPO/.vimrc ~/.vimrc
```

copy tools/aosp_cscope in your PATH

source shell/function.sh in your bashrc or zshrc or ...

## Vim

Vim plugins are managed by vundle. So at first lunch you have to run in vim
```
 :PluginInstall
```


